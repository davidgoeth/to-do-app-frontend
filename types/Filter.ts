import { ToDo } from "./ToDo";

export enum FilterType {
    ALL = "all",
    DONE = "done",
    OPEN = "open"
}