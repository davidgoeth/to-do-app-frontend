export interface ToDo {
  id: number;
  Name?: string;
  Done?: boolean;
}

export type TodoRequest = Omit<ToDo, "id">;