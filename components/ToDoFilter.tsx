import styled from "styled-components";
import { ChangeEvent } from "react";
import { FilterType } from "../types/Filter";

interface ToDoFilterProps {
  options: FilterType[]
  updateFilterType(type: FilterType): void;
}

const TodoFilterWrapper = styled.div`
  margin-bottom: 24px;
  margin-top: 24px;
  width: fit-content;
`;

const SelectWrapper = styled.select`
  margin-left: 8px;
  margin-right: 8px;
`;

export default function ToDoFilter({
  options,
  updateFilterType
}: ToDoFilterProps): JSX.Element {

  const onSelectionChanged = (e: ChangeEvent<HTMLSelectElement>) => updateFilterType(e.target.value as FilterType);

  return (
    <TodoFilterWrapper>
      Show 
      <SelectWrapper onChange={onSelectionChanged}>
        {
          options.map((type: FilterType, index: number) => (
            <option key={index} value={type}>{type}</option>
          ))
        }
      </SelectWrapper> 
      Todos
    </TodoFilterWrapper>
  );
}
