import styled from "styled-components";
import { MouseEvent, useState, ChangeEvent } from "react";
import { TodoRequest } from "../types/ToDo";

interface ToDoNewEntryProps {
  createToDo: (todo: TodoRequest) => void;
}

const ToDoNewEntryWrapper = styled.div`
  margin-bottom: 24px;
  margin-top: 24px;
  width: fit-content;
`;

const InputWrapper = styled.input`
  border-radius: 8px;
  box-shadow: #ccc 2px 2px 3px;
  width: fit-content;
  margin-right: 12px;
  border-color: transparent;
`;

const ButtonWrapper = styled.button`
  border-radius: 8px;
  box-shadow: #ccc 2px 2px 3px;
  width: fit-content;
  background-color: Aquamarine;
  color: black;
  border-color: transparent;
`;

export default function ToDoNewEntry({
  createToDo
}: ToDoNewEntryProps): JSX.Element {

  const [inputValue, setInputValue] = useState<string>('');

  const updateInputValue = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue((_previousValue: string) => event.target.value);
  };

  const addNewTodo = (_e: MouseEvent<HTMLButtonElement>) => createToDo({
    Name: inputValue
  });

  return (
    <ToDoNewEntryWrapper>
      <InputWrapper type="text" placeholder="Name" onChange={ updateInputValue }></InputWrapper>
      <ButtonWrapper onClick={ addNewTodo }>
        Create Todo
      </ButtonWrapper>
    </ToDoNewEntryWrapper>
    
  );
}
