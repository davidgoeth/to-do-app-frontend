import { ToDo } from "../types/ToDo";
import ToDoEntry from "./ToDoEntry";

export interface ToDoListProps {
  toDos: ToDo[];
  updateToDoDoneStatus: (id: number, done: boolean) => void;
}

export function ToDoList({
  toDos,
  updateToDoDoneStatus,
}: ToDoListProps): JSX.Element {
  return (
    <>
      <h1>To-Dos</h1>
      {toDos
        .sort((t1, t2) => +(t1.Done || false) - +(t2.Done || false))
        .map((toDo) => (
          <ToDoEntry
            key={toDo.id}
            toDo={toDo}
            updateToDoDoneStatus={updateToDoDoneStatus}
          />
        ))}
    </>
  );
}
