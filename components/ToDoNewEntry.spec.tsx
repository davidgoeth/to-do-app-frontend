import {
  fireEvent,
  render,
  RenderResult,
  within,
} from "@testing-library/react";
import { TodoRequest } from "../types/ToDo";
import ToDoNewEntry from "./ToDoNewEntry";

describe("ToDoNewEntry", () => {
  let renderResult: RenderResult;
  let button: HTMLButtonElement;
  let input: HTMLInputElement;
  let mockCreateToDoFunction: (todo: TodoRequest) => void;

  beforeEach(() => {
    mockCreateToDoFunction = jest.fn();
    renderResult = render(
      <ToDoNewEntry createToDo={mockCreateToDoFunction}/>
    );

    const {container } = renderResult;
    button = container.querySelector("button") as HTMLButtonElement;
    input = container.querySelector("input[type=text][placeholder=Name]") as HTMLInputElement;
  });

  it("should render a button with a 'Create Todo' text", () => {
    const { container } = renderResult;
    const buttonElement: HTMLButtonElement | null = container.querySelector("button");
    expect(buttonElement).toBeTruthy();
    expect(within(buttonElement as HTMLButtonElement).getByText("Create Todo")).toBeInTheDocument();
  });

  it("should render an input field with a 'Name' placeholder", () => {
    const { container } = renderResult;
    expect(container.querySelector("input[type=text][placeholder=Name]")).toBeTruthy();
  });

  it("should have an empty input value at the beginning", () => {
    expect(input.value).toEqual('');
  });

  it("should call updateTodoList callback with the input value as argument when clicking on the create button", () => {
    const name = 'New ToDo';
    fireEvent.change(input, { target: { value: name}});
    fireEvent.click(button);
    expect(mockCreateToDoFunction).toHaveBeenCalledWith({
      Name: name
    });
  });
});
