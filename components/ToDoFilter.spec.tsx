import {
  fireEvent,
  render,
  RenderResult,
} from "@testing-library/react";
import { FilterType } from "../types/Filter";
import ToDoFilter from "./ToDoFilter";

describe("ToDoFilter", () => {
  let renderResult: RenderResult;
  let selectElement: HTMLSelectElement;
  let mockUpdateFilterFunction: (type: FilterType) => void;
  const filterOptions: FilterType[] = [FilterType.ALL, FilterType.OPEN, FilterType.DONE];

  beforeEach(() => {
    mockUpdateFilterFunction = jest.fn();
    renderResult = render(
      <ToDoFilter options={filterOptions} updateFilterType={mockUpdateFilterFunction}/>
    );

    const {container } = renderResult;
    selectElement = container.querySelector('select') as HTMLSelectElement;
  });

  it("should render a select with options, one option per filter type", () => {
    const { container } = renderResult;
    const options: NodeListOf<Element> = container.querySelectorAll('select option');
    const optionValues: HTMLOptionElement[] = Array.from(options.values()) as HTMLOptionElement[];
    
    for (const optionValue of optionValues) {
      expect(filterOptions.includes(optionValue.value as FilterType)).toBeTruthy();
    }
  });

  it("should call callback with the selected filter type when selection is changed", () => {
    fireEvent.change(selectElement, { target: { value: FilterType.DONE}});
    expect(mockUpdateFilterFunction).toHaveBeenCalledWith(FilterType.DONE);
  });
});
