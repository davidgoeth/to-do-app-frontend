import { ToDoApi } from "./ToDoApi";
import { mockToDos } from "../lib-test/mock-todos";
import { ToDoApiImpl } from "./ToDoApiImpl";
import { ToDo } from "../types/ToDo";

describe("To Do API", () => {
  let toDoApi: ToDoApi;
  let backendUrl = "http://backend";
  let toDoEndpoint = "/to-dos";

  beforeEach(() => (toDoApi = new ToDoApiImpl(backendUrl, toDoEndpoint)));

  it("should fetch all to-dos from an HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest.fn().mockReturnValue(
      Promise.resolve({
        json: () => Promise.resolve(mockToDos),
      }) as Promise<Response>
    );
    global.fetch = fetchMock;

    // Test
    expect(await toDoApi.getAllToDos()).toEqual(mockToDos);
    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`);

    // Cleanup
    fetchMock.mockClear();
  });

  it("should update a to-do using the HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({ json: () => Promise.resolve() }));
    global.fetch = fetchMock;

    const updatedToDo = { id: 0, Name: "new name", Done: false };

    // Test
    await toDoApi.updateToDo(updatedToDo.id, updatedToDo);
    expect(fetchMock).toHaveBeenCalledWith(
      `${backendUrl}${toDoEndpoint}/${updatedToDo.id}`,
      {
        body: JSON.stringify(updatedToDo),
        headers: {
          "Content-Type": "application/json",
        },
        method: "PUT",
      }
    );

    // Cleanup
    fetchMock.mockClear();
  });

  it("should create a to-do using the HTTP endpoint", async () => {
    // Setup
    const createToDoMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({ json: () => Promise.resolve({
        id: 10,
        Name: 'New ToDo',
        Done: false
      }) }));
    global.fetch = createToDoMock;

    const newToDo: Omit<ToDo, "id"> = { Name: "New ToDo", Done: false };

    // Test
    await toDoApi.createToDo(newToDo);
    expect(createToDoMock).toHaveBeenCalledWith(
      `${backendUrl}${toDoEndpoint}`,
      {
        body: JSON.stringify(newToDo),
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
      }
    );

    // Cleanup
    createToDoMock.mockClear();
  });
});
