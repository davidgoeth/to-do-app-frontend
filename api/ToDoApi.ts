import { ToDo } from "../types/ToDo";

export interface ToDoApi {
  getAllToDos(): Promise<ToDo[]>;
  updateToDo(id: number, toDo: ToDo): Promise<void>;
  createToDo(todo: Omit<ToDo, "id">): Promise<ToDo>;
}
