import { ToDoApi } from "./ToDoApi";
import { ToDoApiImpl } from "./ToDoApiImpl";

export class ToDoApiFactory {
  private static toDoApiInstance: ToDoApi;

  /**
   * Factory function to get a {@link ToDoApi} instance.
   */
  static get(): ToDoApi {
    if (!this.toDoApiInstance) {
      this.toDoApiInstance = new ToDoApiImpl();
    }
    return this.toDoApiInstance;
  }
}
