import type { GetServerSidePropsResult, NextPage } from "next";
import { useState } from "react";
import { ToDoList } from "../components/ToDoList";
import { ToDo, TodoRequest } from "../types/ToDo";
import { ToDoApiFactory } from "../api/ToDoApiFactory";
import ToDoNewEntry from "../components/ToDoNewEntry";
import { FilterType } from "../types/Filter";
import ToDoFilter from "../components/ToDoFilter";
import { getAll, getDoneTodos, getOpenTodos } from "../util/FilterFunction";

const toDoApi = ToDoApiFactory.get();

const filterMap = {
  [FilterType.ALL]: getAll,
  [FilterType.OPEN]: getOpenTodos,
  [FilterType.DONE]: getDoneTodos
};

const filterTypes: FilterType[] = [FilterType.ALL, FilterType.DONE, FilterType.OPEN];

interface HomepageProps {
  toDos: ToDo[];
}

export async function getServerSideProps(): Promise<
  GetServerSidePropsResult<HomepageProps>
> {
  return {
    props: {
      toDos: await toDoApi.getAllToDos(),
    },
  };
}

const Home: NextPage<HomepageProps> = ({
  toDos: toDosFromProps,
}: HomepageProps) => {
  const [toDos, setToDos] = useState<ToDo[]>(toDosFromProps);
  const [filterType, setFilterType] = useState<FilterType>(FilterType.ALL);

  async function updateToDoDoneStatus(
    id: number,
    done: boolean
  ): Promise<void> {
    const toDo: ToDo | undefined = toDos.find((toDo) => toDo.id === id);
    if (!toDo) {
      return;
    }

    const updatedToDo: ToDo = {
      ...toDo,
      Done: done,
    };

    setToDos((toDos) => [
      ...toDos.map((toDo) =>
        toDo.id === id && updatedToDo ? updatedToDo : toDo
      ),
    ]);
    await toDoApi.updateToDo(id, updatedToDo);
  }

  async function createToDo(todoRequest: TodoRequest): Promise<void> {
    const todo: ToDo = await toDoApi.createToDo(todoRequest);
    setToDos((toDos) => [
      ...toDos,
      todo
    ]);
  }

  function updateFilterType(type: FilterType): void {
    setFilterType((_previous: FilterType) => type);
  }

  return (
    <>
    <ToDoNewEntry createToDo={createToDo}/>
    <ToDoFilter options={filterTypes} updateFilterType={updateFilterType}/>
    <ToDoList toDos={toDos.filter(filterMap[filterType])} updateToDoDoneStatus={updateToDoDoneStatus} />
    </>);
};

export default Home;
