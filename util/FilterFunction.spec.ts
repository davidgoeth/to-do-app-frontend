import { ToDo } from "../types/ToDo";
import { getAll, getDoneTodos, getOpenTodos } from "./FilterFunction";

describe("FilterFunction", () => {
  let toDos: ToDo[];

  beforeEach(() => {
    toDos = [
      {
        id: 0,
        Name: "To do 1",
        Done: false,
      },
      {
        id: 1,
        Name: "To do 2",
        Done: true,
      },
      {
        id: 2,
        Name: "To do 3",
        Done: false,
      }
    ];
  });

  it("should provide all todos for getAllTodos", () => {
    expect(toDos.filter(getAll)).toEqual(toDos);
  });

  it("should provide only open todos for getOpenTodos", () => {
    expect(toDos.filter(getOpenTodos)).toEqual([
      {
        id: 0,
        Name: "To do 1",
        Done: false,
      },
      {
        id: 2,
        Name: "To do 3",
        Done: false,
      }
    ]);
  });

  it("should provide only done todos for getDoneTodos", () => {
    expect(toDos.filter(getDoneTodos)).toEqual([
      {
        id: 1,
        Name: "To do 2",
        Done: true,
      }
    ]);
  });
});
