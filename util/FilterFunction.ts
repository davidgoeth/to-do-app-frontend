import { ToDo } from "../types/ToDo";

export type FilterFunction<T> = (t: T) => boolean;

export function getAll<T>(_t: T): boolean {
    return true;
} 

export const getDoneTodos: FilterFunction<ToDo> = (todo: ToDo) => !!todo.Done;
export const getOpenTodos: FilterFunction<ToDo> = (todo: ToDo) => !todo.Done;